/**
 * Step 3 - node native tcp client
 */
 import log from '@ajar/marker'
 import net from 'net'
 
 const { port } = process.env;
 
 const socket = net.createConnection({ port }, _ => {
    log.yellow('✨ ⚡connected to server! ⚡✨');
    socket.write('{"initial":"msg, client -> server..."}\n');
 });
 
 socket.on('data', buffer => {
     
     // log.v('data:',buffer.toString());
     
     const {value} = JSON.parse(buffer.toString()); 
 
     if(value && value >= 20){
         socket.end() 
     }else if(value){
       log.v('-> value:',value,' squering...');
       socket.write(Buffer.from(`{"squared":${value**2}}\n`));
     }
 });
   
 socket.on('end', () => { 
   log.yellow('✨ ⚡disconnected from server ⚡✨');
 });